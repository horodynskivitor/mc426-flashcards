# mc426-flashcards
Repositório para projeto de MC426, tratando-se de uma plataforma de flashcards gamificada.

## Sprint 5
Vídeo: [Explicação da Sprint 5](https://youtu.be/Epm7H1CTw-s) 

## Sprint 4
Vídeo: [Explicação da Sprint 4](https://youtu.be/sOC-UkMdsdA) 

## Sprint 3
Vídeo: [Explicação da Sprint 3](https://youtu.be/wJRVYc_mL3s) 

### Guia de inicialização
#### Requisitos
- Python 3 <br>
- PyQt 5 <br>
- PyInstaller 3.6 ou superior (apenas para executável)

#### Executando pelo Script 
Abra a pasta src e execute o script initGUI.py, o programa executará e já poderá ser usado.
#### Executando por executável
Execute o seguinte comando a partir da pasta src
##### GNU/Linux
`pyinstaller initGUI.py --name "Flash cards" -w --onedir --add-data "./GUI/designs/:./GUI/designs/" --add-data "./GUI/images/:./GUI/images"`
##### Windows 
`pyinstaller initGUI.py --name "Flash cards" -w --onedir --add-data "./GUI/designs/;./GUI/designs/" --add-data "./GUI/images/;./GUI/images"` <br>
<br>
Será gerado um arquivo executável dentro da pasta dist, execute-o, o programa iniciará e já poderá ser utilizado.


## Descrição da Arquitetura

### Diagrama em Nível de Componentes

![](image/C4.png)

### Estilo de elaboração da arquitetura

O estilo de arquitetura adotado foi o estilo de camadas. O projeto inclui 3 níveis de abstração que são, da camada mais baixa para a mais alta:

1. A classe DataManager que interage diretamente com o banco de dados da aplicação. Consome os dados que estão nas tabelas do banco de dados, e fornece dados para este mesmo banco a partir de métodos de inserção de dados.

2. A camada de interação com a classe DataManager, representadas pelas classes Deck e Card. Além de abstrair completamente a interação com a classe do banco de dados, estas classes gerenciam os dados vindos da interação com o banco e os fornece para a camada seguinte de maneira facilmente consumível para a próxima camada.

3. A interface gráfica, representada por um conjunto de classes do componente Flashcard GUI, que consome os dados da camada de interação para exibir cartões para o usuário. Além disso, fornece novos dados também para a camada anterior para a inserção e remoção de cartões.

### Descrição textual dos principais componentes

Como demonstrado pelo diagrama em nível de componentes, existem 4 componentes principais:

1. DataManager: componente que interage diretamente com o banco de dados da aplicação. Contem métodos para gerenciar cartões de maneira geral (inserção, remoção, edição e busca tanto de cartões como de baralhos). Seus dados de retorno não são formatados, deixando esta responsabilidade para outros componentes.

2. FlashCard: representa um cartão que pertence a algum baralho. Interage com a classe DataManager para obter cartões específicos, formatando os dados de retorno desta camada a ser consumida pela próxima classe.

3. Deck: representa um baralho, com referências aos diversos cartões que contem. Conversa com a DataManager para obter uma lista de referências aos diversos cartões que pertencem a um determinado baralho, que pode ser usada para obter os diversos cartões dentro de um baralho.

4. Flashcard GUI: um conjunto de classes que gerencia a exibição da estrutura de cartões para o usuário, usando os componentes AddCard, AddDeck e CardView. Consome os dados de Card e Deck para tanto, e recebe entradas do usuário para determinar qual dos dados do usuário deve ser mostrado em um momento, como a lista de baralhos ou algum cartão específico.
