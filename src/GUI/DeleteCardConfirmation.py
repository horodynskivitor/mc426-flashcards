from PyQt5.Qt import Qt

from GUI.DeleteDeckConfirmation import DeleteDeckConfirmation

class DeleteCardConfirmation(DeleteDeckConfirmation):
    def __init__(self, parent, card, deck):
        super().__init__(parent)
        self.parent = parent
        self.card = card
        self.deck = deck

        self.tela.confirmButton.clicked.disconnect()
        self.tela.cancelButton.clicked.disconnect()
        self.tela.confirmButton.clicked.connect(self.__confirm)
        self.tela.cancelButton.clicked.connect(self.__cancel)
        self.tela.label.setText("Tem certeza que deseja deletar esse cartão?")

        self.tela.setWindowModality(Qt.ApplicationModal)
        self.show()


    def show(self):
        self.tela.show()

    def __confirm(self):
        self.deck.delCard(self.card)
        self.parent.updateDeletedCard()
        self.tela.close()

    
    def __cancel(self):
        self.tela.close()