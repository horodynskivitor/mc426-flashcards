from PyQt5 import uic, QtWidgets, QtCore
from deck.deck import Deck
import os
from PyQt5.QtWidgets import QMessageBox

class AddDeckMenu():
    def __init__(self, mainMenu):
        self.dir_path = os.path.dirname(os.path.realpath(__file__))
        self.Tela = uic.loadUi("./GUI/designs/addDeckMenu.ui")
        self.Tela.setWindowFlags(QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.WindowMinimizeButtonHint)

        self.Tela.pushButton.clicked.connect(self.cancelButton) #cancel button
        self.Tela.pushButton_2.clicked.connect(self.createDeckButton) #create button

        self.Tela.label_2.hide() #hide "Deck already exists"

        self.mainMenu = mainMenu

    
    def showAddDeckMenu(self):
        self.Tela.show()

    def cancelButton(self):
        self.Tela.close()

    def createDeckButton(self):
        deckName = self.Tela.lineEdit.text()
        if deckName == "":
            return

        if not(self._deckExists(deckName)): #check if deck exists

            newDeck = Deck(name=deckName)
            self.mainMenu.updateDeckVisualization()
            self.Tela.close()

        else:
            self.Tela.label_2.show()

    def _deckExists(self, name):
        decks = Deck.getDeckList()
        match = list(filter(lambda x: x["name"].lower() == name.lower(), decks))
        if match == []:
            return False
        return True
        