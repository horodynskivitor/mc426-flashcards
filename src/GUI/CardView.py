from PyQt5 import uic, QtWidgets, QtCore
from PyQt5.Qt import Qt
from PyQt5.QtGui import QIcon
import os
import random

from deck.deck import Deck
from deck.flashcard import FlashCard
from GUI.EditStandardCard import EditStandardCard
from GUI.DeleteCardConfirmation import DeleteCardConfirmation


class CardView(QtWidgets.QMainWindow):
    def __init__(self, idDeck):
        super().__init__()

        self.deck = Deck(idDeck)
        self.side = 'Front'  
        self.cards = Deck.getCardList(self.deck)
        self.total = len(self.cards)

        self.deleted = []

        self.__loadWindow()
        self.__buttonsSetup()
        
    def switchSide(self):
        self.Tela.setFocus()
        if self.side == 'Front':
            card = self.deck.getCard(self.cards[self.order[self.position]])
            self.Tela.cardText.setText(card.back)
            self.side = 'Back'
            
        else:
            card = self.deck.getCard(self.cards[self.order[self.position]])
            self.Tela.cardText.setText(card.front)
            self.side = 'Front'
        
        self.switchCardColor(self.side)


    def showCardView(self):
        self.Tela.show()

    def navigateCard(self, jump):
        self.position = self.position + jump
        if self.position == self.total:
            self.position = self.position - self.total
                
        elif self.position == -1:
            self.position = self.total - 1
        
        card = self.deck.getCard(self.cards[self.order[self.position]])
        self.Tela.cardText.setText(card.front)
        self.side = 'Front'
        self.switchCardColor(self.side)
        
    def keyPressEvent(self, e):
        if self.total != 0:
            if e.key() == Qt.Key_Right:
                self.navigateCard(1)
                
            elif e.key() == Qt.Key_Left:
                self.navigateCard(-1)

            elif e.key() == Qt.Key_Space:
                self.switchSide()

    def switchCardColor(self, side):
        if side == 'Front':
            self.Tela.cardText.setStyleSheet("background-color: rgb(226, 226, 226);")
        else:
            self.Tela.cardText.setStyleSheet("background-color: rgb(157, 152, 226);")

    def __loadWindow(self):
        self.dir_path = os.path.dirname(os.path.realpath(__file__))
        self.Tela = uic.loadUi("./GUI/designs/cardView.ui", self)
        self.Tela.info.setIcon(QIcon("./GUI/images/info.png"))
        self.Tela.setFocus()

        self.Tela.lcd.hide()

    def __buttonsSetup(self):
        #connects buttons and methods
        if self.total != 0:
            self.position = 0
            self.order = random.sample(range(0, self.total), self.total)
            self.Tela.cardText.setText(self.deck.getCard(self.cards[self.order[self.position]]).front)
            self.Tela.fowardButton.clicked.connect(lambda: self.navigateCard(1))
            self.Tela.backButton.clicked.connect(lambda: self.navigateCard(-1))
            self.Tela.viewAnswer.clicked.connect(self.switchSide)
            self.Tela.editCard.clicked.connect(self.__editCard)
            self.Tela.deleteCard.clicked.connect(self.__deleteCard)
        else:
            self.Tela.cardText.setText('Não há cards nesse deck!')

        self.Tela.settingsButton.hide()

        
    def __editCard(self):
        card = self.deck.getCard(self.cards[self.order[self.position]])
        self.editWindow = EditStandardCard(card, self)

    def __deleteCard(self):
        card = self.cards[self.order[self.position]]
        self.deleteWindow = DeleteCardConfirmation(self, card, self.deck)

    def updateCardContent(self):
        self.cards = Deck.getCardList(self.deck)
        card = self.deck.getCard(self.cards[self.order[self.position]])
        print("front: {}, back: {}".format(card.front, card.back))
        if self.side == 'Front':
            self.Tela.cardText.setText(card.front)
        else:
            self.Tela.cardText.setText(card.back)

    def updateDeletedCard(self):
        self.total -= 1
        if self.total == 0:
            self.Tela.cardText.setText('Não há cards nesse deck!')
            self.Tela.fowardButton.clicked.disconnect()
            self.Tela.backButton.clicked.disconnect()
            self.Tela.viewAnswer.clicked.disconnect()
            self.Tela.editCard.clicked.disconnect()
            self.Tela.deleteCard.clicked.disconnect()

        else:
            del self.order[self.position]
            self.navigateCard(0)

