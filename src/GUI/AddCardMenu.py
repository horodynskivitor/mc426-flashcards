from PyQt5 import uic, QtWidgets, QtCore
import os
from GUI.AddStandardCard import AddStandardCard
from GUI.AddCardWindowFactory import *

class AddCardMenu():
    def __init__(self):
        self.dir_path = os.path.dirname(os.path.realpath(__file__))
        self.Tela = uic.loadUi("./GUI/designs/addCardMenu.ui")
        self.Tela.setWindowFlags(QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.WindowMinimizeButtonHint)

        self.Tela.radioButton.setChecked(True) #botao 1 padrao

        self.Tela.pushButton.clicked.connect(self.confirmButton) #botao de confirmacao
        self.Tela.pushButton_2.clicked.connect(self.closeWindow) #botao de cancelamento
    
    def showAddCardMenu(self):
        self.Tela.show()

    def confirmButton(self):
        if self.Tela.radioButton.isChecked(): #botão 1
            self.factory = StandardCardWindowFactory()
        else:
            return 
        self.cardWindow = self.factory.createWindow()
        self.cardWindow.showWindow()
        self.closeWindow()

    def closeWindow(self):
        self.Tela.close()