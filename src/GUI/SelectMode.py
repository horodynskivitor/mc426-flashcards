from PyQt5 import uic, QtWidgets, QtCore
from deck.deck import Deck
import os

from PyQt5.QtWidgets import QMessageBox
from GUI.CardView import CardView
from GUI.TimeTrialView import TimeTrialView
from GUI.CardListView import CardListView

class SelectMode():
    def __init__(self, idDeck):
        self.dir_path = os.path.dirname(os.path.realpath(__file__))
        self.Tela = uic.loadUi("./GUI/designs/selectMode.ui")
        self.Tela.setWindowFlags(QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.WindowMinimizeButtonHint)

        self.Tela.radioButton.setChecked(True)

        self.Tela.cancelButton.clicked.connect(self.cancelButton) 
        self.Tela.confirmButton.clicked.connect(self.confirmButton) 

        self.idDeck = idDeck

    def showSelectMode(self):
        self.Tela.show()

    def cancelButton(self):
        self.Tela.close()

    def closeWindow(self):
        self.Tela.close()

    def confirmButton(self):
        if self.Tela.radioButton.isChecked(): 
            self.cardView = CardView(self.idDeck)
            self.cardView.showCardView()

        elif self.Tela.radioButton_2.isChecked():
            self.timeTrialView = TimeTrialView(self.idDeck)
            self.timeTrialView.showCardView()

        elif self.Tela.radioButton_3.isChecked():
            self.cardListView = CardListView(self.idDeck)
            self.cardListView.showCardView()

        self.closeWindow()
        