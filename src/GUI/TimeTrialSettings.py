from PyQt5 import uic, QtWidgets, QtCore
from PyQt5.QtCore import QSettings
import os

class TimeTrialSettings():
    def __init__(self, parent):
        self.parent = parent
        self.dir_path = os.path.dirname(os.path.realpath(__file__))
        self.Tela = uic.loadUi("./GUI/designs/timeTrialSettings.ui")
        self.Tela.setWindowFlags(QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.WindowMinimizeButtonHint)
        self.Tela.lineEdit.setText(str(self.parent.timePerQuestion))

        self.settings = QSettings('Flashcards', 'TimeTrialValues')

        self.Tela.pushButton.clicked.connect(self.confirmButton) #botao de confirmacao
        self.Tela.pushButton_2.clicked.connect(self.closeWindow) #botao de cancelamento
    
    def showTimeTrialSettings(self):
        self.Tela.show()

    def confirmButton(self):
        try:
            newTime = int(self.Tela.lineEdit.text())
            if newTime > 0:
                self.settings.setValue('Time', newTime)
                self.parent.timePerQuestion = int(self.Tela.lineEdit.text())
                self.parent.updateTime()
                self.closeWindow()
            else:
                self.Tela.label_2.setText('Insira um número inteiro e positivo')
        except:
            self.Tela.label_2.setText('Insira um número inteiro e positivo')

    def closeWindow(self):
        self.Tela.close()