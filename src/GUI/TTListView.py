from PyQt5 import uic, QtWidgets, QtCore
from PyQt5.Qt import Qt

from GUI.TTListItem import TTListItem
from GUI.EditStandardCard import EditStandardCard
from GUI.DeleteCardConfirmation import DeleteCardConfirmation
from deck.deck import Deck

class TTListView(QtWidgets.QMainWindow):
    #Aqui deck é uma lista de tuplas contendo o card e um bool indicando seu acerto/erro
    def __init__(self, deck, idDeck):
        super().__init__()

        self.idDeck = idDeck
        self.deck = deck
        self.tela = uic.loadUi("./GUI/designs/CardListView.ui", self)

        self.tela.deleteButton.hide()
        self.tela.editButton.hide()

        self.tela.deckTitle.setText(Deck.getDeckName(idDeck))

        self._populateCardList()
        self._calcPerformance()


    def _populateCardList(self):
        for item in self.deck:
            cardItem = TTListItem(item)
            listItem = QtWidgets.QListWidgetItem(self.tela.cardList)
            listItem.setSizeHint(cardItem.sizeHint())

            self.tela.cardList.addItem(listItem)
            self.tela.cardList.setItemWidget(listItem, cardItem)

    def _calcPerformance(self):
        n = len(self.deck)
        totalCorrect = 0
        for i in range(n):
            if self.deck[i][1]:
                totalCorrect += 1
        try:
            perf = totalCorrect/n
        except ZeroDivisionError:
            perf = 1
        self.tela.deckPerformance.setText(
            "Desempenho geral: {:.2f}%".format(perf*100)
        )


    def showTTListView(self):
        self.tela.show()


