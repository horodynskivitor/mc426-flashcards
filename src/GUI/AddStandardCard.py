from PyQt5 import uic, QtWidgets, QtCore
import os
from deck.deck import Deck
from GUI.AddCard import AddCard

class AddStandardCard(AddCard):
    def __init__(self):
        super().__init__("./GUI/designs/addStandardCard.ui")

        self.Tela.cancelButton.clicked.connect(self.closeWindow) #cancel button
        self.Tela.label_3.hide() #hide error label

        #fill decks selector
        self.deckList = Deck.getSortedDeckList()
        self.__fillDeckSelector()

        self.Tela.addButton.clicked.connect(self.__addCard) #add card connector



    def __addCard(self):
        #get content
        deckName = self.Tela.deckSel.currentText()
        frontText = self.Tela.frontText.toPlainText()
        backText = self.Tela.backText.toPlainText()
        if (deckName == '' or frontText == '' or backText == ''):
            self.Tela.label_3.show()
            return
        
        #get deck id
        id = Deck.getDeckByName(deckName)
    
        #instantiate deck and add card
        deck = Deck(id=id)
        _ = deck.addCard(frontText, backText)

        self.Tela.close()


    def __fillDeckSelector(self):
        if len(self.deckList) == 0:
            self.Tela.label_3.setText("Crie um deck primeiro")
            self.Tela.label_3.show()

        for i in self.deckList:
            self.Tela.deckSel.addItem(i["name"])




