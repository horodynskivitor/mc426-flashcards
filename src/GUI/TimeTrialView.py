from PyQt5 import uic, QtWidgets, QtCore
from PyQt5.Qt import Qt
from deck.deck import Deck
from GUI.CardView import CardView
from GUI.TimeTrialSettings import TimeTrialSettings
from GUI.CardListView import CardListView
from GUI.TTListView import TTListView
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QSettings
import sys

class TimeTrialView(CardView):
    def __init__(self, idDeck):
        super().__init__(idDeck)

        self.timePerQuestion = 5
        self.idDeck = idDeck
        self.cardList = []

        self.settings = QSettings('Flashcards', 'TimeTrialValues')
        try:
            self.timePerQuestion = int(self.settings.value('Time'))
        except:
            pass
        
        self.paused = True
        self.timer = QtCore.QTimer()
        self.deckSize = len(Deck.getCardList(self.deck))
        self.time =  self.deckSize * self.timePerQuestion
        if self.time > 999:
            self.Tela.lcd.setDigitCount(4)
        if self.total != 0:
            self.__adjustPage()
        else: 
            pass

    def __adjustPage(self):
        self.Tela.lcd.show()
        self.Tela.fowardButton.setText('Acertei')
        self.Tela.backButton.setText('Errei')

        self.Tela.backButton.clicked.disconnect()
        self.Tela.backButton.clicked.connect(lambda: self.navigateCard(False))
        self.Tela.fowardButton.clicked.disconnect()
        self.Tela.fowardButton.clicked.connect(lambda: self.navigateCard(True))
        self.Tela.viewAnswer.clicked.disconnect()
        self.Tela.viewAnswer.clicked.connect(self._startGame)
        self.Tela.settingsButton.clicked.connect(self._changeSettings)

        self.Tela.lcd.display(self.time) 
        self.Tela.editCard.hide()
        self.Tela.deleteCard.hide()
        self.Tela.fowardButton.hide()
        self.Tela.backButton.hide()
        self.Tela.settingsButton.show()
        self.Tela.info.hide()
        self.Tela.cardText.setText('Pressione o botão para começar')
        self.Tela.viewAnswer.setText('Começar')
        self.Tela.settingsButton.setIcon(QIcon("./GUI/images/settings_icon.png"))

        self.timer.timeout.connect(self.__updateTimer)
        

    def closeWindow(self):
        self.Tela.close()

    def __updateTimer(self):
        if self.time == 0:
            self.__endGame()
        else:
            self.time = self.time - 1
        self.Tela.lcd.display(self.time) 

    def __endGame(self):
        self.Tela.fowardButton.hide()
        self.Tela.backButton.hide()
        self.Tela.lcd.hide()
        self.Tela.viewAnswer.setText('Fechar')
        self.Tela.cardText.setText('Fim de jogo')
        self.Tela.editCard.setText('Visão geral')
        self.Tela.editCard.show()
        self.Tela.deleteCard.setText('Detalhes da última tentativa')
        self.Tela.deleteCard.show()
        
        self.Tela.viewAnswer.clicked.disconnect()
        self.Tela.viewAnswer.clicked.connect(self.closeWindow)
        self.Tela.editCard.clicked.disconnect()
        self.Tela.editCard.clicked.connect(self._deckStats)
        self.Tela.deleteCard.clicked.disconnect()
        self.Tela.deleteCard.clicked.connect(self._sessionStats)

    def navigateCard(self, acerto):
        card = self.deck.getCard(self.cards[self.order[self.position]])
        card.playCard(acerto)
        self.cardList.append((card, acerto))
        if self.position != self.total - 1:
            self.position = self.position + 1
            card = self.deck.getCard(self.cards[self.order[self.position]])
            self.Tela.cardText.setText(card.front)
            self.side = 'Front'
            self.switchCardColor(self.side)
            self.Tela.fowardButton.hide()
            self.Tela.backButton.hide()

        else:
            self.__endGame()


    def keyPressEvent(self, e):
        if self.total != 0:
            if e.key() == Qt.Key_Space:
                self.switchSide()


    def switchSide(self):
        self.Tela.setFocus()
        self.Tela.fowardButton.show()
        self.Tela.backButton.show()
        if self.side == 'Front':
            card = self.deck.getCard(self.cards[self.order[self.position]])
            self.Tela.cardText.setText(card.back)
            self.side = 'Back'
            
        else:
            card = self.deck.getCard(self.cards[self.order[self.position]])
            self.Tela.cardText.setText(card.front)
            self.side = 'Front'
        
        self.switchCardColor(self.side)

    def togglePause(self):
        if self.paused:
            self.timer.start(1000)
            self.paused = False
        else:
            self.timer.stop()
            self.paused = True

    def _startGame(self):
        self.timer.start(1000)
        self.Tela.viewAnswer.clicked.disconnect()
        self.Tela.viewAnswer.clicked.connect(self.switchSide)
        self.Tela.cardText.setText(self.deck.getCard(self.cards[self.order[self.position]]).front)
        self.Tela.viewAnswer.setText('Virar o cartão')
        self.Tela.settingsButton.hide()
        
        self.togglePause()

    def _changeSettings(self):
        self.settingsWindow = TimeTrialSettings(self)
        self.settingsWindow.showTimeTrialSettings()

    def updateTime(self):
        self.time = self.deckSize * self.timePerQuestion
        if self.time > 999:
            self.Tela.lcd.setDigitCount(4)
        self.Tela.lcd.display(self.time) 
    
    def _deckStats(self):
        self.statsView = CardListView(self.idDeck)
        self.statsView.showCardView()

    def _sessionStats(self):
        self.sessionView = TTListView(self.cardList, self.idDeck)
        self.sessionView.showTTListView()