from PyQt5 import QtCore, QtGui, QtWidgets

MAXCHAR = 40


class CardListItem(QtWidgets.QWidget):
    '''
    Custom widget to show in the CardListView
    '''

    def __init__(self, card, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.card = card

        self.layoutV = QtWidgets.QVBoxLayout()
        self.layoutH = QtWidgets.QHBoxLayout()

        self.text = self.__textLabel()
        self.correct, self.wrong, self.accuracy = self.__cardPerformance()
        self.__buildLayout()
        self.__applyStyleSheet()

    def __textLabel(self):
        text = QtWidgets.QLabel()
        cardText = self.card.front
        if len(cardText) > MAXCHAR:
            cardText = cardText[0:MAXCHAR-1] + "..."
        text.setText(cardText)
        return text

    def __cardPerformance(self):
        card = self.card

        correct = card.correct
        wrong = card.played - correct
        if correct+wrong == 0:
            accuracy = "100%"
        else:
            accuracy = correct/(correct+wrong)
            accuracy = str(round(accuracy*100, 2))

        accuracy = "Desempenho: " + accuracy + '%'

        correct = QtWidgets.QLabel("Acertos: " + str(correct))
        wrong = QtWidgets.QLabel("Erros: " + str(wrong))
        accuracy = QtWidgets.QLabel(str(accuracy))

        return correct, wrong, accuracy

    def __buildLayout(self):
        self.layoutH.addWidget(self.correct)
        self.layoutH.addWidget(self.wrong)
        self.layoutH.addWidget(self.accuracy)

        self.layoutV.addWidget(self.text)
        self.layoutV.addLayout(self.layoutH)

        self.setLayout(self.layoutV)

    def __applyStyleSheet(self):
        self.text.setStyleSheet('''
            border-color: rgb(5, 243, 255);
            color: rgb(255, 251, 226);
            font: 12pt "MS Shell Dlg 2";
        ''')

        self.correct.setStyleSheet('''
            border-color: rgb(5, 243, 255);
            color: rgb(15, 189, 32);
            font: 12pt "MS Shell Dlg 2";
        ''')

        self.wrong.setStyleSheet('''
            border-color: rgb(5, 243, 255);
            color: rgb(250, 77, 77);
            font: 12pt "MS Shell Dlg 2";
        ''')

        self.accuracy.setStyleSheet('''
            border-color: rgb(5, 243, 255);
            color: rgb(220, 228, 242);
            font: 12pt "MS Shell Dlg 2";
        ''')
