from PyQt5 import QtCore, QtGui, QtWidgets

MAXCHAR = 40


class TTListItem(QtWidgets.QWidget):
    '''
    Custom widget to show in the TTListView
    '''
    #info é uma tupla contendo o card e um bool que indica acerto/erro desse card
    def __init__(self, info, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.info = info
        self.card = info[0]

        if info[1]:
            self.acerto = QtWidgets.QLabel('Acertou')
        else:
            self.acerto = QtWidgets.QLabel('Errou')

        self.layoutV = QtWidgets.QVBoxLayout()
        self.layoutH = QtWidgets.QHBoxLayout()
        self.text = self.__textLabel()
        self.__buildLayout()
        self.__applyStyleSheet()

    def __textLabel(self):
        text = QtWidgets.QLabel()
        cardText = self.card.front
        if len(cardText) > MAXCHAR:
            cardText = cardText[0:MAXCHAR-1] + "..."
        text.setText(cardText)
        return text

    def __buildLayout(self):
        self.layoutH.addWidget(self.acerto)

        self.layoutV.addWidget(self.text)
        self.layoutV.addLayout(self.layoutH)

        self.setLayout(self.layoutV)

    def __applyStyleSheet(self):
        self.text.setStyleSheet('''
            border-color: rgb(5, 243, 255);
            color: rgb(255, 251, 226);
            font: 12pt "MS Shell Dlg 2";
        ''')
        if self.info[1]:
            self.acerto.setStyleSheet('''
                border-color: rgb(5, 243, 255);
                color: rgb(15, 189, 32);
                font: 12pt "MS Shell Dlg 2";
            ''')
        else:
            self.acerto.setStyleSheet('''
                border-color: rgb(5, 243, 255);
                color: rgb(250, 77, 77);
                font: 12pt "MS Shell Dlg 2";
            ''')
