from PyQt5 import uic, QtWidgets, QtCore
from PyQt5.Qt import Qt

from GUI.CardListItem import CardListItem
from GUI.EditStandardCard import EditStandardCard
from GUI.DeleteCardConfirmation import DeleteCardConfirmation
from deck.deck import Deck

class CardListView(QtWidgets.QMainWindow):
    def __init__(self, idDeck):
        super().__init__()

        self.idDeck = idDeck
        self.tela = uic.loadUi("./GUI/designs/CardListView.ui", self)

        self.deck = Deck(idDeck)
        self.tela.deckTitle.setText(Deck.getDeckName(idDeck))

        self._populateCardList()
        self._calcPerformance()
        self._connectWidgets()


    def _populateCardList(self):
        self.cardListId = self.deck.getCardList()

        for id in self.cardListId:
            card = self.deck.getCard(id)
            cardItem = CardListItem(card)
            listItem = QtWidgets.QListWidgetItem(self.tela.cardList)
            listItem.setSizeHint(cardItem.sizeHint())

            self.tela.cardList.addItem(listItem)
            self.tela.cardList.setItemWidget(listItem, cardItem)

    def _calcPerformance(self):
        cardList = self.tela.cardList
        n = cardList.count()
        totalPlayed = 0
        totalCorrect = 0
        for i in range(n):
            item = cardList.item(i)
            card = cardList.itemWidget(item).card
            p = card.played
            c = card.correct
            totalPlayed += p
            totalCorrect += c

        try:
            perf = totalCorrect/totalPlayed
        except ZeroDivisionError:
            perf = 1
        self.tela.deckPerformance.setText(
            "Desempenho geral: {:.2f}%".format(perf*100)
        )


    def _connectWidgets(self):
        tela = self.tela

        tela.deleteButton.hide()
        tela.editButton.hide()
        tela.deleteButton.clicked.connect(self._deleteCard)
        tela.editButton.clicked.connect(self._editCard)
        tela.cardList.itemSelectionChanged.connect(self.__itemSelected)

    def _deleteCard(self):
        listItem = self.tela.cardList.selectedItems()[0]
        cardId = self.tela.cardList.itemWidget(listItem).card.id
        self.deleteWindow = DeleteCardConfirmation(self, cardId, self.deck)

    def updateDeletedCard(self):
        listItem = self.tela.cardList.selectedItems()[0]
        row = self.tela.cardList.row(listItem)
        _ = self.tela.cardList.takeItem(row)


    def _editCard(self):
        listItem = self.tela.cardList.selectedItems()[0]
        card = self.tela.cardList.itemWidget(listItem).card
        self.editWindow = EditStandardCard(card, self)

    
    def updateCardContent(self):
        listItem = self.tela.cardList.selectedItems()[0]
        card = self.tela.cardList.itemWidget(listItem).card
        cardItem = CardListItem(card)
        self.tela.cardList.setItemWidget(listItem, cardItem)


    def __itemSelected(self):
        self.tela.editButton.show()
        self.tela.deleteButton.show()

    def showCardView(self):
        self.tela.show()


