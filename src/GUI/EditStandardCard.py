from PyQt5 import uic, QtWidgets, QtCore
from PyQt5.Qt import Qt
import os
from deck.deck import Deck
from GUI.AddStandardCard import AddStandardCard

class EditStandardCard(AddStandardCard):
    def __init__(self, card, parent):
        super().__init__()
        self.card = card
        self.parent = parent
        self.__adjustPage()
        self.Tela.setWindowModality(Qt.ApplicationModal)

        self.showWindow()

    def __adjustPage(self):
        self.Tela.frontText.setText(self.card.front)
        self.Tela.backText.setText(self.card.back)
        self.Tela.deckSel.hide()
        self.Tela.label_4.hide()
        self.Tela.addButton.setText("Salvar")
        self.Tela.addButton.clicked.disconnect()
        self.Tela.addButton.clicked.connect(self.__editCard)
        self.Tela.cancelButton.clicked.disconnect()
        self.Tela.cancelButton.clicked.connect(self.__editCardCancel)

    def __editCard(self):
        #get content
        frontText = self.Tela.frontText.toPlainText()
        backText = self.Tela.backText.toPlainText()
        if (frontText == '' or backText == ''):
            self.Tela.label_3.show()
            return

        self.card.editCard(frontText, backText)
        self.parent.updateCardContent()
        self.Tela.close()

    def __editCardCancel(self):
        self.Tela.close()
