from PyQt5 import uic, QtWidgets, QtCore
import os
from deck.deck import Deck
from abc import ABC

class AddCard(ABC):
    def __init__(self, designFile):
        self.dir_path = os.path.dirname(os.path.realpath(__file__))
        self.Tela = uic.loadUi(designFile)
        self.Tela.setWindowFlags(QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.WindowMinimizeButtonHint)

    def showWindow(self):
        self.Tela.show()

    def closeWindow(self):
        self.Tela.close()
