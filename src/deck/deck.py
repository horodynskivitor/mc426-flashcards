import deck.DataManager as DM
from deck.flashcard import FlashCard


class Deck:
    __DtMa = DM.DataManager()

    # initializes the Deck
    # Input: a optional Deck Id, a optional Deck name
    # Output: None
    def __init__(self, id=None, name=None):
        if id == None and name == None:
            raise RuntimeError("specify deck id OR name")
        if id != None:
            self.id = id
        else:
            self.id = Deck.__DtMa.insertNewDeck(name)
            if self.id == None:
                print("Error on Deck creation")


    # Destroys Deck
    # Input: None
    # Output: None
    def __del__(self):
        pass

    # static method that gets the list of all decks from data manager and returns it
    # Input: None
    # Output: the list of decks
    @staticmethod
    def getDeckList():
        listDecks = Deck.__DtMa.getDecks()
        listDecks = Deck.__createDict(listDecks)
        return listDecks

    # static method that converts a list of tuples of decks in a list of dicts of dekcs
    # Input: a list of Tuple
    # Output: a list of dicts
    @staticmethod
    def __createDict(listTuple):
        listDict = []
        for i in listTuple:
            dicta = {
                "id": i[0],
                "name": i[1]
            }
            listDict.append(dicta)
        return listDict

    # static method that gets the list of all decks from data manager and returns it, already sorted
    # Input: None
    # Output: a list of dicts
    @staticmethod
    def getSortedDeckList():
        decks = Deck.getDeckList()
        return sorted(decks, key=lambda x: x["name"].lower())

    # static method that returns the deck id, given its name
    # Input: the name of the deck
    # Output: the deck id, if it exists, None, otherwise
    @staticmethod
    def getDeckByName(name):
        deckList = Deck.getDeckList()
        try:
            deck = list(filter(lambda x: x["name"].lower() == name.lower(), deckList))[0]
            return deck["id"]
        except:
            return None

    @staticmethod
    def getDeckName(id):
        deckList = Deck.getDeckList()
        try:
            deck = list(filter(lambda x: x["id"] == id, deckList))[0]
            return deck["name"]
        except:
            return None



    # A method that gets the list of decks from a deck
    # Input: None
    # Output: a list of card Ids
    def getCardList(self):
        aux = []
        cardsList = Deck.__DtMa.getCardsFromDeck(self.id)
        for i in cardsList:
            aux.append(i[0])
        return aux

    # A method that adds a card to a deck
    # Input: front and back from a card
    # Output a Flashcard
    def addCard(self, front, back):
        card = FlashCard(front, back, deckID=self.id)
        return card

    # A method that gets a card from the data manager
    # Input: a card Id
    # Output: a Flashcard with the researched card
    def getCard(self, cardID):
        flashcard = FlashCard(cardID=cardID)
        return flashcard

    # A method that deletes a card given its id
    # Input: a card Id
    def delCard(self, cardID):
        Deck.__DtMa.deleteCard(cardID)

    # A method that delete a Deck and all of its cards from database
    # Input: None
    # Output: None
    def delDeck(self):
        Deck.__DtMa.deleteDeck(self.id)
        self.__del__()

    # A method that calls database to edit a deck
    # Input: The new name of a Deck
    # Output: None
    def editDeck(self, newName):
        Deck.__DtMa.editDeck(self.id, newName)

    # A method that copy a card from a given ID to this Deck
    # Input: the ID of the card I want to copy
    # Output: the new copied FlashCard
    def copyCard(self, cardID):
        newID = Deck.__DtMa.copyCard(cardID, self.id)
        newCard = FlashCard(cardID=newID)
        return newCard
