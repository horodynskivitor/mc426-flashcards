import unittest

from deck.deck import Deck
from deck.DataManager import DataManager

class TestDeck(unittest.TestCase):

    def setUp(self):
        # remove all data before testing
        dm = DataManager()
        dm.purgeDatabase()  

    ##### Manipulacao de Decks #####

    def test_deckCreation(self):
        deck = Deck(name="name")
        self.assertEqual(deck.getDeckList()[0]["name"], "name")


    @unittest.expectedFailure
    def test_deckCreationNoName(self):
        deck = Deck()
        self.assertIsInstance(deck, Deck)

    @unittest.expectedFailure
    def test_deckCreationInvalidName(self):
        deck = Deck(name=[1,2,3])
        self.assertIsInstance(deck, Deck)


    def test_getSortedDeckList(self):
        for i in range(3, 0, -1):
            deck = Deck(name="name-{}".format(i))

        decks = list(map(lambda x: x['name'], deck.getSortedDeckList()))
        self.assertEqual(decks, ["name-{}".format(i) for i in range(1, 4)])


    def test_getDeckById(self):
        deck1 = Deck(name="HAHA")
        deck1.addCard("frente", "verso")
        id = deck1.id
        sameDeck = Deck(id=id)

        cardList = sameDeck.getCardList()
        card = sameDeck.getCard(cardList[0])
        self.assertEqual(card.front, "frente")
        self.assertEqual(card.back, "verso")

    @unittest.expectedFailure
    def test_getDeckByInvalidId(self):
        deck1 = Deck(name="HAHA")
        deck1.addCard("frente", "verso")
        id = deck1.id
        sameDeck = Deck(id=id+1)

        cardList = sameDeck.getCardList()
        card = sameDeck.getCard(cardList[0])
        self.assertEqual(card.front, "frente")
        self.assertEqual(card.back, "verso")

    def test_editDeck(self):
        deck = Deck(name="name")
        deck.editDeck("novoNome")
        self.assertEqual(deck.getDeckList()[0]["name"], "novoNome")

    @unittest.expectedFailure
    def test_editDeckInvalidArg(self):
        deck = Deck(name="name")
        deck.editDeck({"a":1})
        self.assertEqual(deck.getDeckList()[0]["name"], {"a":1})


    def test_delDeck(self):
        deck = Deck(name="name")
        deck.delDeck()
        self.assertEqual(Deck.getDeckList(), [])

    def test_getDeckByName(self):
        deck = Deck(name="name")

        self.assertEqual(deck.id, Deck.getDeckByName("name"))

    def test_getDeckByWrongName(self):
        deck = Deck(name="name")

        self.assertIsNone(Deck.getDeckByName("nadadame"))




    ##### Manipulacao de cards #####

    def test_getCard(self):
        deck = Deck(name="name")
        deck.addCard("frente", "verso")
        card = deck.getCard(1)
        self.assertEqual(card.front, "frente")
        self.assertEqual(card.back, "verso")

    @unittest.expectedFailure
    def test_getCardwrongId(self):
        deck = Deck(name="name")
        deck.addCard("frente", "verso")
        card = deck.getCard(2)
        self.assertEqual(card.front, "frente")
        self.assertEqual(card.back, "verso")

    @unittest.expectedFailure
    def test_getCardInvalidId(self):
        deck = Deck(name="name")
        deck.addCard("frente", "verso")
        card = deck.getCard("str")
        self.assertEqual(card.front, "frente")
        self.assertEqual(card.back, "verso")

    def test_addCard(self):
        deck = Deck(name="name")
        deck.addCard("frente", "verso")
        cardList = deck.getCardList()
        card = deck.getCard(cardList[0])
        self.assertEqual(card.front, "frente")
        self.assertEqual(card.back, "verso")

    @unittest.expectedFailure
    def test_addCardLessArgument(self):
        deck = Deck(name="name")
        deck.addCard("frente")
        cardList = deck.getCardList()
        card = deck.getCard(cardList[0])
        self.assertEqual(card.front, "frente")
        self.assertEqual(card.back, "verso")

    @unittest.expectedFailure
    def test_addCardNotString(self):
        deck = Deck(name="name")
        deck.addCard("frente", {"a":5})
        cardList = deck.getCardList()
        card = deck.getCard(cardList[0])
        self.assertEqual(card.front, "frente")
        self.assertEqual(card.back, {"a":5})

    def test_deleteCard(self):
        deck = Deck(name="name")
        deck.addCard("frente", "verso")
        cardList = deck.getCardList()
        card = deck.getCard(cardList[0])
        self.assertEqual(card.front, "frente")
        self.assertEqual(card.back, "verso")
        deck.delCard(cardList[0])
        cardList = deck.getCardList()
        self.assertEqual(cardList, [])

    def test_deleteCardWrongId(self):
        deck = Deck(name="name")
        deck.addCard("frente", "verso")
        cardList = deck.getCardList()
        card = deck.getCard(cardList[0])
        self.assertEqual(card.front, "frente")
        self.assertEqual(card.back, "verso")
        deck.delCard(cardList[0]+1)
        cardList = deck.getCardList()
        card = deck.getCard(cardList[0])
        self.assertEqual(card.front, "frente")
        self.assertEqual(card.back, "verso")


    
if __name__ == '__main__':
    unittest.main()