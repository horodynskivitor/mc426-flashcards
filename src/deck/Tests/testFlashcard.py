import unittest
from deck.DataManager import DataManager as DM
from deck.deck import Deck
from deck.flashcard import FlashCard

class TestFlashCard(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        x = DM()
        x.purgeDatabase()
    
    # Expected Sucesss
    def test_newCard(self):
        newDeck = Deck(name="DeckInit")
        newCard = FlashCard("Front1!", "Back1!", newDeck.id)
        self.assertEqual(newCard.front, "Front1!")
        self.assertEqual(newCard.back, "Back1!")
        self.assertEqual(newCard.played, 0)
        self.assertEqual(newCard.correct, 0)
        newDeck.delDeck()

    def test_instCard(self):
        newDeck = Deck(name="DeckInst")
        newCard = FlashCard("Front1!", "Back1!", newDeck.id)
        sameCard = FlashCard(cardID = newCard.id)
        self.assertEqual(sameCard.front, newCard.front)
        self.assertEqual(sameCard.back, newCard.back)
        self.assertEqual(sameCard.played, newCard.played)
        self.assertEqual(sameCard.correct, newCard.correct)
        newDeck.delDeck()

    def test_editCard(self):
        newDeck = Deck(name="DeckEdit")
        newCard = FlashCard("Front1!", "Back1!", newDeck.id)
        newCard.editCard("NewFront1!")
        self.assertEqual(newCard.front, "NewFront1!")
        newCard.editCard("NewFront2!", "NewBack2!")
        self.assertEqual(newCard.front, "NewFront2!")
        self.assertEqual(newCard.back, "NewBack2!")
        newDeck.delDeck()

    def test_playCardRight(self):
        newDeck = Deck(name="DeckPlay")
        newCard = FlashCard("Front1!", "Back1!", newDeck.id)
        newCard.playCard(True)
        self.assertEqual(newCard.played, 1)
        self.assertEqual(newCard.correct, 1)
        newCard.playCard(False)
        self.assertEqual(newCard.played, 2)
        self.assertEqual(newCard.correct, 1)
        newDeck.delDeck()

    @classmethod
    def tearDownCards(self):
        x = DM()
        x.purgeDatabase()

if __name__ == '__main__':
    unittest.main()

