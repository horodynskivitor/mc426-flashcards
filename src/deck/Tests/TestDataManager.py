import unittest
import random
from deck.DataManager import DataManager

x = DataManager()


class TestDataManager(unittest.TestCase):

    def setUp(self):
        x.purgeDatabase()

    def test_deck_insertion(self):
        deckid1 = x.insertNewDeck('alemão')
        deckid2 = x.insertNewDeck('inglês')
        self.assertEqual(x._getTable('decks'), [
                         (1, 'alemão'), (2, 'inglês')])
        deckid3 = x.insertNewDeck('inglês')
        self.assertEqual(x._getTable('decks'), [
                         (1, 'alemão'), (2, 'inglês')])
        self.assertEqual(deckid3, None)

    def test_card_insertion(self):
        deckid1 = x.insertNewDeck('alemão')
        deckid2 = x.insertNewDeck('inglês')
        cards = []
        cards.append(x.insertNewCard(
            "Wie geht's?", "Como vai?", deckid1))
        cards.append(x.insertNewCard(
            "Gesuntheit!", "Saúde!", deckid1))
        cards.append(x.insertNewCard(
            "Ein, Zwei, Drei", "Um, dois, três", deckid1))
        cardNone = x.insertNewCard('hello', 'world', cards[2]+1)
        cards.append(x.insertNewCard(
            'Hello World!', 'Olá Mundo!', deckid2))
        cards.append(x.insertNewCard('Something', 'Algo', deckid2))
        cards.append(x.insertNewCard('Game', 'Jogo', deckid2))
        cards.append(x.insertNewCard(
            'Computer', 'Computador', deckid2))

        for i, c in enumerate(cards):
            self.assertEqual(i+1, c)
        self.assertEqual(cardNone, None)

    def test_get_cardids_from_deck(self):
        deckid1 = x.insertNewDeck('alemão')
        deckid2 = x.insertNewDeck('inglês')
        cardsDeck1 = []
        cardsDeck2 = []
        cardsDeck1.append(x.insertNewCard(
            "Wie geht's?", "Como vai?", deckid1))
        cardsDeck2.append(x.insertNewCard('Game', 'Jogo', deckid2))
        cardsDeck2.append(x.insertNewCard(
            'Computer', 'Computador', deckid2))
        cardsDeck1.append(x.insertNewCard(
            "Gesuntheit!", "Saúde!", deckid1))
        cardsDeck2.append(x.insertNewCard(
            'Hello World!', 'Olá Mundo!', deckid2))
        cardsDeck2.append(x.insertNewCard(
            'Something', 'Algo', deckid2))
        cardsDeck1.append(x.insertNewCard(
            "Ein, Zwei, Drei", "Um, dois, três", deckid1))
        for i, c in enumerate(x.getCardsFromDeck(deckid1)):
            card = c[0]
            self.assertEqual(card, cardsDeck1[i])
        for i, c in enumerate(x.getCardsFromDeck(deckid2)):
            card = c[0]
            self.assertEqual(card, cardsDeck2[i])
        self.assertEqual([], x.getCardsFromDeck(deckid2+1))

    def test_deck_deleting(self):
        deckid1 = x.insertNewDeck('alemão')
        cardsDeck1 = []
        cardsDeck1.append(x.insertNewCard(
            "Wie geht's?", "Como vai?", deckid1))
        cardsDeck1.append(x.insertNewCard(
            "Gesuntheit!", "Saúde!", deckid1))
        cardsDeck1.append(x.insertNewCard(
            "Ein, Zwei, Drei", "Um, dois, três", deckid1))
        x.deleteDeck(deckid1)
        self.assertEqual(x._getTable('decks'), [])
        self.assertEqual(x.getCardsFromDeck(deckid1), [])
        deckid1 = x.insertNewDeck('alemão')
        deckid2 = x.insertNewDeck('inglês')
        cardsDeck1.append(x.insertNewCard(
            "Wie geht's?", "Como vai?", deckid1))
        cardsDeck1.append(x.insertNewCard(
            "Gesuntheit!", "Saúde!", deckid1))
        cardsDeck1.append(x.insertNewCard(
            "Ein, Zwei, Drei", "Um, dois, três", deckid1))
        x.deleteDeck(deckid2)
        self.assertEqual(x._getTable('decks'), [(deckid1, 'alemão')])
        for i, c in enumerate(x.getCardsFromDeck(deckid1)):
            card = c[0]
            self.assertEqual(card, cardsDeck1[i])

    def test_card_editing(self):
        deckid1 = x.insertNewDeck('alemão')
        cardsDeck1 = []
        cardsDeck1.append(x.insertNewCard(
            "Wie geht's?", "Como vai?", deckid1))
        cardsDeck1.append(x.insertNewCard(
            "Gesuntheit!", "Saúde!", deckid1))
        cardsDeck1.append(x.insertNewCard(
            "Ein, Zwei, Drei", "Um, dois, quatro", deckid1))
        x.editCard(cardsDeck1[2], None, "Um, dois, três")
        self.assertEqual(x.getCard(cardsDeck1[2]), [
                         ('Ein, Zwei, Drei', 'Um, dois, três', 0, 0)])
        x.editCard(cardsDeck1[2], "1,2,3", None)
        self.assertEqual(x.getCard(cardsDeck1[2]), [
                         ('1,2,3', 'Um, dois, três', 0, 0)])
        x.editCard(
            cardsDeck1[2], "Ein, Zwei, Drei, Vier", "Um, dois, três, quatro")
        self.assertEqual(x.getCard(cardsDeck1[2]), [
                         ('Ein, Zwei, Drei, Vier', 'Um, dois, três, quatro', 0, 0)])

    def test_deck_updating(self):
        deckid1 = x.insertNewDeck('alemão')
        deckid2 = x.insertNewDeck('inglês')
        x.editDeck(deckid1, "deutsch")
        self.assertEqual(x._getTable('decks'), [(1, 'deutsch'), (2, 'inglês')])
        x.editDeck(deckid2+1, "nonsense")
        self.assertEqual(x._getTable('decks'), [(1, 'deutsch'), (2, 'inglês')])

    def test_card_copy(self):
        deckid1 = x.insertNewDeck('alemão')
        deckid2 = x.insertNewDeck('inglês')
        cardsDeck1 = []
        cardsDeck2 = []
        cardsDeck1.append(x.insertNewCard(
            "Wie geht's?", "Como vai?", deckid1))
        cardsDeck2.append(x.insertNewCard('Game', 'Jogo', deckid2))
        cardsDeck2.append(x.insertNewCard(
            'Computer', 'Computador', deckid2))
        newcardid = x.copyCard(cardsDeck1[0], deckid2)
        self.assertEqual(x.getCard(newcardid), [
                         ("Wie geht's?", 'Como vai?', 0, 0)])

    def test_play_card(self):
        deckid1 = x.insertNewDeck('alemão')
        cardid1 = x.insertNewCard(
            "Wie geht's?", "Como vai?", deckid1)
        self.assertEqual(x.getCard(cardid1), [
                         ("Wie geht's?", 'Como vai?', 0, 0)])
        x.playCard(cardid1, True)
        x.playCard(cardid1, True)
        x.playCard(cardid1, True)
        x.playCard(cardid1, True)
        self.assertEqual(x.getCard(cardid1), [
                         ("Wie geht's?", 'Como vai?', 4, 4)])
        x.playCard(cardid1, False)
        x.playCard(cardid1, False)
        x.playCard(cardid1, False)
        x.playCard(cardid1, False)
        self.assertEqual(x.getCard(cardid1), [
                         ("Wie geht's?", 'Como vai?', 8, 4)])
        x.playCard(cardid1, False)
        x.playCard(cardid1, True)
        x.playCard(cardid1, False)
        x.playCard(cardid1, False)
        self.assertEqual(x.getCard(cardid1), [
                         ("Wie geht's?", 'Como vai?', 12, 5)])

    def test_removeCard(self):
        deckid1 = x.insertNewDeck('alemão')
        cardid1 = x.insertNewCard(
            "Wie geht's?", "Como vai?", deckid1)

        cards = x.getCardsFromDeck(deckid1)
        self.assertEqual(cards, [(cardid1,)])
    
        x.deleteCard(cardid1)

        cards = x.getCardsFromDeck(deckid1)
        self.assertEqual(cards, [])

    def test_removeWrongCardId(self):
        deckid1 = x.insertNewDeck('alemão')
        cardid1 = x.insertNewCard(
            "Wie geht's?", "Como vai?", deckid1)

        cards = x.getCardsFromDeck(deckid1)
        self.assertEqual(cards, [(cardid1,)])
    
        x.deleteCard(cardid1+1)

        cards = x.getCardsFromDeck(deckid1)
        self.assertEqual(cards, [(cardid1,)])


if __name__ == '__main__':
    unittest.main()
