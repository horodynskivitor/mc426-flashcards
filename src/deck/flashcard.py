import deck.DataManager as DM

class FlashCard:
    __DtMa = DM.DataManager()

    # Initializes a Card
    # Input: a optional front, back, deckID, and cardID
    # Output: Card
    # How it works:
    #   - Create a new card: card = FlashCard(front = front, back = back, deckID = deckID)
    #   - Instantiate a new card: card = FlashCard(cardID = cardID)
    def __init__(self, front = None, back = None, deckID = None , cardID = None):
        if cardID == None:
            self.front = front
            self.back = back
            self.played = 0
            self.correct = 0
            self.id = FlashCard.__DtMa.insertNewCard(front, back, deckID)
        else:
            self.id = cardID
            info = FlashCard.__DtMa.getCard(cardID)
            self.front = info[0][0]
            self.back = info[0][1]
            self.played = info[0][2]
            self.correct = info[0][3]

    # Edits a cards front or back
    # Input: A cards id, the new front (can be None) and the new back (can also be None)
    # Output: True if all changes could have been made and false otherwise
    def editCard(self, newFront, newBack = None):
        can_edit = True
        if ((newFront == None and newBack == None) or (newFront == '' or newBack == '')):
            print("Error: could not edit this card. Both front and back are empty")
            can_edit = False
        elif newFront != None and newBack != None:
            self.front = newFront
            if newBack != None:
             self.back = newBack
        elif newFront != None:
            self.front = newFront
        elif newBack != None:
            self.back = newBack
        if (FlashCard.__DtMa.editCard(self.id, newFront, newBack)):
            can_edit = True
        else:
            print("Error: could not edit this card.")
            can_edit = False
        return can_edit

    # Inform DataManager that the card was played
    # Input: Boolean to inform if it was correct (True) or incorrect (False)
    # Output: None
    def playCard(self, right):
        self.played, self.correct = FlashCard.__DtMa.playCard(self.id, right)
