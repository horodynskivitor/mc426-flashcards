### Ponto A: [Unidade, Funcionalidade, Caixa Preta]

Teste de unidade para testar as funcionalidades da classe que gerencia o banco de dados para testar suas diversas funções (remoção, inserção, atualização e busca). Por abstratir a funcionalidade interna da classe, se trata de um teste caixa preta

### Ponto B: [Sistema, Usabilidade, Funcional]

Testes para garantir a qualidade da UX do programa.

### Ponto C: [Integração, Funcionalidade, Caixa branca]

Testar a comunicação entre as diversas interfaces dos componentes do projeto, visto que a arquitetura do projeto é de camadas.

### Ponto D: [Sistema, Funcionalidade, Caixa preta]

Em geral, muitas das funcionalidades de unidades, como as do banco de dados, envolvem o sistema todo, desde o usário enviar o pedido, toda a comunicação dos componentes, até o acesso ao banco de dados. Por isso é necessário validar se o sistema como um todo consegue realizar essas atividades.

### Ponto E: [Sistema, Segurança, Caixa Preta]

Teste a nível de sistema para a segurança. Como o risco da aplicação é baixo (por se tratar de flashcards) e o principal ponto de falha é a nível de sistema, basta aplicar um teste deste nível para testar a segurança